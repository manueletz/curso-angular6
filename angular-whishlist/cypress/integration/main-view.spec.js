describe('ventana principal', () => {
  it('tiene encabezado correcto y en español por defecto', () => {
    cy.visit('http://localhost:4200');
    cy.contains('angular-whishlist');
    cy.get('h1 b').should('contain', 'HOLA es');
  });
});

/// <reference types="Cypress" />

context('Window', () => {
  beforeEach(() => {
    //cy.visit('https://example.cypress.io/commands/window')
    cy.visit('http://localhost:4200/home')
  })

  it('cy.window() - get the global window object', () => {
    // https://on.cypress.io/window
    cy.window().should('have.property', 'top')
  })

  it('cy.document() - get the document object', () => {
    // https://on.cypress.io/document
    cy.document().should('have.property', 'charset').and('eq', 'UTF-8')
  })

  it('cy.title() - get the title', () => {
    // https://on.cypress.io/title
    cy.title().should('include', 'AngularWhishlist')
  })
})